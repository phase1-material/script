#!/bin/bash
# setup script for partMuon_BT2016
# Author : kosuke.takeda@cern.ch

# If you test use online software packages at a new environment, you may have to modify the follwoings : 
#  1) WORKAREA      = This is the path to online software location.
#  2) TDAQ_VERSION  = This is the TDAQ package version which will be used.
#  3) CMTCONFIG     = This is the compiler version. Please check the pc-tbed-pub OS version.

umask 0002

# Configuratinos
BASEDIR=$(cd $(dirname $BASH_SOURCE); pwd)
tmp=''
while read line
do
  export WORKAREA=$line
done < ${BASEDIR}/LOCAL_WORKAREA

export TDAQ_VERSION=tdaq-07-01-00
export CMTCONFIG=x86_64-slc6-gcc62-opt
#export CMTCONFIG=x86_64-centos7-gcc62-opt
export TDAQ_PARTITION=partMuon_BT2016
export LCG_INST_PATH=/afs/cern.ch/atlas/project/tdaq/inst/

# ====================================================== #
#  Load configurations for CMake
# ====================================================== #
source /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh ${TDAQ_VERSION} ${CMTCONFIG}

# ====================================================== #
#  The following environment variables will be used 
#  in some oks files.
# ====================================================== #
export PHASE1_OKS=${WORKAREA}/OksForTestBed     # the oks-files location
export PHASE1_SL_CONF_PATH=${WORKAREA}/bitfile/ # the bit-file location
export PHASE1_PATGEN_PATH=${PHASE1_OKS}/data/   # the testpattern.data location
export PHASE1_SW=${WORKAREA}/online             # Online packages
export PHASE1_INST_PATH=${PHASE1_SW}/installed  # Binary files after the compile

# Muon software setup
export CMTPROJECTPATH=${PHASE1_SW}:/afs/cern.ch/atlas/offline/external:/afs/cern.ch/atlas/project/tdaq/cmt/

# ====================================================== #
#  Python, Java
# ====================================================== #
export PATH=$TDAQ_PYTHON_HOME/bin:$PATH
export JAVA_HOME=$TDAQ_JAVA_HOME

# Cernlib setup
export CERN=$LCG_INST_PATH/external/cernlib
export CERN_LEVEL=2006a/$LCGPLAT
export CERN_ROOT=$CERN/$CERN_LEVEL

# ====================================================== #
#  Path and library path
# ====================================================== #
export PATH=$PATH:$HOME/bin
export PATH=$PHASE1_INST_PATH/$CMTCONFIG/bin:$PATH
export PATH=$JAVA_HOME/bin:$PATH:$ROOTSYS/bin
export PATH=$PATH:$CERN_ROOT/bin

export LD_LIBRARY_PATH=$PHASE1_INST_PATH/$CMTCONFIG/lib:$TDAQ_INST_PATH/$CMTCONFIG/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib

# ====================================================== #
#  Database location (i.e. OKS files)
# ====================================================== #
export TDAQ_DB_PATH=${PHASE1_INST_PATH}/share/data/:${PHASE1_OKS}:/tbed/oks/${TDAQ_VERSION}/:${TDAQ_DB_PATH}
export TDAQ_DB_DATA=${PHASE1_OKS}/partitions/${TDAQ_PARTITION}.data.xml

alias rundaq='setup_daq -p $TDAQ_PARTITION -d  $TDAQ_DB_DATA'


echo ">>>> Welcome to partMuon_BT2016 (${TDAQ_VERSION} environment) <<<<"
