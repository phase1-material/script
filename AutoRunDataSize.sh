#!/bin/sh

if [ "$1" = '' ]; then
  echo "The argument is needed! [Length]"
  exit 0
fi

if [ "$2" = '' ]; then
  echo "The argument is incorrect! [Time]"
  exit 0
fi


# delete old shared memories
ls /dev/shm/* | xargs -n1 unlink

# NONE --> INITIAL
rc_waitstate -p ${TDAQ_PARTITION} -n RootController NONE
rc_sender -p ${TDAQ_PARTITION} -n RootController -c INITIALIZE

# INITIAL --> CONFIGURED
rc_waitstate -p ${TDAQ_PARTITION} -n RootController INITIAL
rc_sender -p ${TDAQ_PARTITION} -n RootController -c CONFIGURE

# CONFIGURED --> CONNECTED
rc_waitstate -p ${TDAQ_PARTITION} -n RootController CONFIGURED
rc_sender -p ${TDAQ_PARTITION} -n RootController -c CONNECT

# CONNECTED --> RUNNING
rc_waitstate -p ${TDAQ_PARTITION} -n RootController CONNECTED

# Do the script for controling data size
Length=$1
ssh sbctgc-jvme-10 "cd ~/workspace/public/atlas-l1muon/endcap/; source script/setup.sh; ./script/DataControler.sh $Length"
echo "-- Script was done for controling data size. The expected data size is `expr $Length \* 4 + 3` word"

rc_sender -p ${TDAQ_PARTITION} -n RootController -c START

SleepTime=1
iCount=1;
Time=$2
while :
do
  sleep $SleepTime
  arg=`expr $SleepTime \* $iCount`
  printf '\r%3d' $arg
  ((iCount++))
  if [ $iCount -gt $Time ]; then 
    break
  fi
done
printf '\n'

# RUNNING --> STOP
rc_waitstate -p ${TDAQ_PARTITION} -n RootController RUNNING
rc_sender -p ${TDAQ_PARTITION} -n RootController -c STOP

# STOP --> CONFIGURED
rc_waitstate -p ${TDAQ_PARTITION} -n RootController CONNECTED
rc_sender -p partMuon_BT2016 -n RootController -c UNCONFIG

# CONFIGURED --> NONE
rc_waitstate -p ${TDAQ_PARTITION} -n RootController INITIAL
rc_sender -p partMuon_BT2016 -n RootController -c SHUTDOWN
