#!/bin/sh

# You can control the average L1A rates.
# Average 100 kHz -> 0x190

# Old version SL
# vme_poke 0x21d014 0x190 2 2
# New version SL
#vme_poke 0xf1d014 0x190 2 2
#vme_poke 0xf2d014 0x190 2 2
#vme_poke 0xf3d014 0x190 2 2
#vme_poke 0xf4d014 0x190 2 2
#vme_poke 0xf5d014 0x190 2 2
#vme_poke 0xf6d014 0x190 2 2
#vme_poke 0xf7d014 0x190 2 2
#vme_poke 0xf8d014 0x190 2 2
#vme_poke 0xf9d014 0x190 2 2
#vme_poke 0xfad014 0x190 2 2
#vme_poke 0xfbd014 0x190 2 2

### Switch random data mode
vme_poke 0xf1d014 0x2 2 2
vme_poke 0xf2d014 0x2 2 2
vme_poke 0xf3d014 0x2 2 2
vme_poke 0xf4d014 0x2 2 2
vme_poke 0xf5d014 0x2 2 2
vme_poke 0xf6d014 0x2 2 2
vme_poke 0xf7d014 0x2 2 2
vme_poke 0xf8d014 0x2 2 2
vme_poke 0xf9d014 0x2 2 2
vme_poke 0xfad014 0x2 2 2
vme_poke 0xfbd014 0x2 2 2
