#!/bin/sh

if [ "$1" = '' ]; then
  echo "An argument is needed."
  echo "[Time] : if you want to run the system for 60 seconds,"
  echo "         you have to give the 60 as an argument."
  exit 0
fi


# delete old shared memories
ls /dev/shm/* | xargs -n1 unlink

# NONE --> INITIAL
rc_waitstate -p ${TDAQ_PARTITION} -n RootController NONE
rc_sender -p ${TDAQ_PARTITION} -n RootController -c INITIALIZE

# INITIAL --> CONFIGURED
rc_waitstate -p ${TDAQ_PARTITION} -n RootController INITIAL
rc_sender -p ${TDAQ_PARTITION} -n RootController -c CONFIGURE

# CONFIGURED --> CONNECTED
rc_waitstate -p ${TDAQ_PARTITION} -n RootController CONFIGURED
rc_sender -p ${TDAQ_PARTITION} -n RootController -c CONNECT

# CONNECTED --> RUNNING
rc_waitstate -p ${TDAQ_PARTITION} -n RootController CONNECTED
rc_sender -p ${TDAQ_PARTITION} -n RootController -c START

# How long do you want to run the system?
SleepTime=1
iCount=1;
Time=$1
while :
do
  sleep $SleepTime
  arg=`expr $SleepTime \* $iCount`
  printf '\r%3d' $arg
  ((iCount++))
  if [ $iCount -gt $Time ]; then 
    break
  fi
done
printf '\n'

# RUNNING --> STOP
rc_waitstate -p ${TDAQ_PARTITION} -n RootController RUNNING
rc_sender -p ${TDAQ_PARTITION} -n RootController -c STOP

# STOP --> CONFIGURED
rc_waitstate -p ${TDAQ_PARTITION} -n RootController CONNECTED
rc_sender -p partMuon_BT2016 -n RootController -c UNCONFIG

# CONFIGURED --> NONE
rc_waitstate -p ${TDAQ_PARTITION} -n RootController INITIAL
rc_sender -p partMuon_BT2016 -n RootController -c SHUTDOWN
