
CURRENT_AREA=${PWD}

# ------ Move to the installatoin are ------
INSTALLATION_AREA=/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-l1muon/endcap/
cd ${INSTALLATION_AREA}

PACKAGE=(L1MuESLModule L1MuESRODModule L1MuESRODSoftware L1MuESwCore L1MuETTCCtrlModule L1MuETTCFanoutModule L1MuEVmeApi)
cd online/
for item in ${PACKAGE[@]}; do
    cd ${item}
    
    # check whether the repository is clean or not
    git status | grep "clean" > /dev/null
    if [ $? -ne 0 ]; then 
     echo `git status`
     echo -e ""
    fi
    
    # check the branch name  
    branch=`git branch -a | grep -e \*`
    echo -e "[${item}] ${branch}"
    
    # check whether using tag or not 
    hash=`git log --decorate --oneline | head -1`
    if [[ ${hash} =~ tag ]]; then
     tag_name=  echo ${hash} | sed -r 's/^.*('${item}'-..-..-..).*/\1/'
     echo -e ""
    else
     echo -e "${hash}"
     echo -e ""
    fi
#echo -e ""
    cd ..
done

cd ../OksForTestBed
# check whether the repository is clean or not
git status | grep "clean" > /dev/null
if [ $? -ne 0 ]; then 
echo `git status`
echo -e ""
fi
branch=`git branch -a | grep -e \*`
echo -e "[OksForTestBed] ${branch}"
hash=`git log --decorate --oneline | head -1`
if [[ ${hash} =~ tag ]]; then
tag_name=  echo ${hash} | sed -r 's/^.*(OksForTestBed-..-..-..).*/\1/'
echo -e ""
else
echo -e "${hash}"
echo -e ""
fi

cd ../PCIeSLINKDriver
# check whether the repository is clean or not
git status | grep "clean" > /dev/null
if [ $? -ne 0 ]; then 
echo `git status`
echo -e ""
fi
branch=`git branch -a | grep -e \*`
echo -e "[PCIeSLINKDriver] ${branch}"
hash=`git log --decorate --oneline | head -1`

if [[ ${hash} =~ tag ]]; then
tag_name=  echo ${hash} | sed -r 's/^.*(PCIeSLINKDriver-..-..-..).*/\1/'
echo -e ""
else
echo -e "${hash}"
echo -e ""
fi

cd ${CURRENT_AREA}
