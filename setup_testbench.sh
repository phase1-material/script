#!/bin/sh

# Load setup script
source ./script/setup.sh

# Directory
DIR_SW=online
DIR_OKS=OksForTestBed
DIR_BIT=bitfile

mkdir $DIR_SW $DIR_OKS $DIR_BIT

# Git clone
cd $DIR_SW
git clone -b develop-tbed ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESRODSoftware
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuEVmeApi
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESwCore
git clone -b develop-tbed ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESRODModule
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuETTCFanoutModule
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuETTCCtrlModule
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESLModule

# Prepare to use cmake
touch CMakeLists.txt
echo "cmake_minimum_required(VERSION 3.6.0)"          > CMakeLists.txt
echo "find_package(TDAQ)"                            >> CMakeLists.txt
echo "include(CTest)"                                >> CMakeLists.txt
echo "tdaq_project(myproject 1.0.0 USES tdaq 7.1.0)" >> CMakeLists.txt

cmake_config
cd $CMTCONFIG
make -j8
make install
