#!/bin/sh

# git clone 
git clone -b develop-tbed ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESRODSoftware
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuEVmeApi
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESwCore
git clone -b develop-tbed ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESRODModule
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuETTCFanoutModule
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuETTCCtrlModule
git clone -b develop      ssh://git@gitlab.cern.ch:7999/atlas-l1muon/endcap/online/L1MuESLModule
