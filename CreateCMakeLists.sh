#!/bin/sh

touch CMakeLists.txt
echo "cmake_minimum_required(VERSION 3.6.0)"          > CMakeLists.txt
echo "find_package(TDAQ)"                            >> CMakeLists.txt
echo "include(CTest)"                                >> CMakeLists.txt
echo "tdaq_project(myproject 1.0.0 USES tdaq 7.1.0)" >> CMakeLists.txt
