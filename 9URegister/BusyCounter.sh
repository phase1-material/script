#!/bin/sh

if [ -z ${TDAQ_PARTITION} ]; then
 source /afs/cern.ch/user/k/ktakeda/workspace/public/Phase1Software/scripts/setup_partMuon_tdaq-06-01-01.sh > /dev/null
fi

echo ">>>>>>>>>>>>>>>>>>>>>>>>> BUSY COUNTER <<<<<<<<<<<<<<<<<<<<<<<<<<<"
echo "TTC-Fanout board : vme_peek 0x11014 2 2 --> `vme_peek 0x11014 2 2`"
echo "SL-01 board      : vme_peek 0x11044 2 2 --> `vme_peek 0x11044 2 2`"
echo "SL-02 board      : vme_peek 0x11054 2 2 --> `vme_peek 0x11054 2 2`"
echo "SL-03 board      : vme_peek 0x11064 2 2 --> `vme_peek 0x11064 2 2`"
echo "SL-04 board      : vme_peek 0x11074 2 2 --> `vme_peek 0x11074 2 2`"
echo "SL-05 board      : vme_peek 0x11084 2 2 --> `vme_peek 0x11084 2 2`"
echo "SL-06 board      : vme_peek 0x11094 2 2 --> `vme_peek 0x11094 2 2`"
echo "SL-07 board      : vme_peek 0x110a4 2 2 --> `vme_peek 0x110a4 2 2`"
echo "SL-08 board      : vme_peek 0x110b4 2 2 --> `vme_peek 0x110b4 2 2`"
echo "SL-09 board      : vme_peek 0x110c4 2 2 --> `vme_peek 0x110c4 2 2`"
echo "SL-10 board      : vme_peek 0x110d4 2 2 --> `vme_peek 0x110d4 2 2`"
echo "SL-11 board      : vme_peek 0x110e4 2 2 --> `vme_peek 0x110e4 2 2`"
echo "SL-12 board      : vme_peek 0x110f4 2 2 --> `vme_peek 0x110f4 2 2`"
echo "SROD             : vme_peek 0x11174 2 2 --> `vme_peek 0x11174 2 2`"
