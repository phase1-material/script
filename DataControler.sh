#!/bin/bash
#for monitoring test

# DataControler.sh <board address> <Number <= 7>
# This script can be used to control data length from a NewSL board.

# Data size = <Number> x <32-bit> x <Readout BC>
# Number : a third argument
# Readout BC : OKS parameters

# Board address
# Address=("0x0021" "0x00f1" "0x00f2" "0x00f3" "0x00f4" "0x00f5" "0x00f6" "0x00f7" "0x00f8" "0x00f9" "0x00fa" "0x00fb")
Address=("0x0021" "0x00f1" "0x00f2" "0x00f3" "0x00f4" "0x00f6" "0x00f7" "0x00f8" "0x00f9" "0x00fa" "0x00fb")

DataSize=$1

function ControlDataSize() {
boardAddress=$1
width=$2
cpld=0
fpga=4
bus_addr=002
testdata_addr=80
tp_len_addr=360
tp_wait_addr=362

BUS=$boardAddress$bus_addr$cpld
LEN=$boardAddress$tp_len_addr$fpga
WAIT=$boardAddress$tp_wait_addr$fpga

vme_poke $BUS 0x125 2 2

usleep 10

# time of test data != 0
vme_poke $LEN 0xffff 2 2

usleep 1

# time+1 of test data = 0
vme_poke $WAIT 0x0 2 2

usleep 1

a=1

if [ $width -gt 7 ]; then
  echo "Error"
else

  while [ $a -le $width ]
  do

    TD=$boardAddress$testdata_addr$a$fpga

    vme_poke $TD 0x1 2 2
    usleep 10

    (( a++ ))
  done

fi
}

function Loop(){
for iBoardAddress in ${Address[@]}; do
  #echo "Board address[$iBoardAddress] has been configured. Number=[${DataSize}] "
  echo "ControlDataSize ${iBoardAddress} ${DataSize}"
  ControlDataSize ${iBoardAddress} ${DataSize}
done
}

Loop

